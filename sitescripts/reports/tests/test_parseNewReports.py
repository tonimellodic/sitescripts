# This file is part of the Adblock Plus web scripts,
# Copyright (C) 2006-present eyeo GmbH
#
# Adblock Plus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# Adblock Plus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys

import pytest
import py.path

# We have to prevent the attempt to import MySQLdb -- we don't have it in the
# test environment and we'd rather not add it.
sys.modules['MySQLdb'] = sys

from sitescripts.reports.bin import parseNewReports


DATA_PATH = py.path.local(__file__).dirpath('data')
REPORTS = {
    name: DATA_PATH.join(name).read()
    for name in [
        'report-abp-3.8.xml',
        'report-abp-3.9.xml',
        'broken-screenshot.xml',
    ]
}


@pytest.fixture()
def report_file(tmpdir, name):
    path = tmpdir.join(name)
    path.write(REPORTS[name])
    return str(path)


def executeQuery_mock(cursor, *_):
    # This will be called in processReport to check for duplicates.
    cursor.fetchone.return_value = None


@pytest.mark.parametrize('name', REPORTS)
def test_processReport(name, report_file, mocker):
    """Test report loading and saving into the database."""
    mocker.patch('sitescripts.subscriptions.knownIssuesParser.findMatches')
    prefix = 'sitescripts.reports.bin.parseNewReports.'
    mocker.patch(prefix + 'get_db')
    mocker.patch(prefix + 'executeQuery', executeQuery_mock)
    saveReport_mock = mocker.patch(prefix + 'saveReport')

    parseNewReports.processReport(report_file)
    call_args = saveReport_mock.call_args
    assert call_args[0][0] == os.path.splitext(name)[0]

    if name == 'broken-screenshot.xml':
        assert 'screenshot' not in call_args[0][1]
    else:
        assert 'screenshot' in call_args[0][1]
