notifications
=============

Backend for Adblock Plus notifications. It provides the following URLs:

* */notification.json* - Return notifications to show

See [notification specification](https://gitlab.com/eyeo/specs/spec/blob/master/spec/abp/notifications.md) for more details.

Required packages
-----------------

* [mock](https://pypi.python.org/pypi/mock) (Only for the tests)
